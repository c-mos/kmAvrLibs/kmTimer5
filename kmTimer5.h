/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file kmTimer5.h
* @mainpage
* @section pageTOC Content
* @brief The kmTimer5 library provides functions to initialize and control Timer 5 on AVR MCUs.
* - kmTimer5.h
* - kmTimer5DefaultConfig.h
*
*  **Created on**: 3/15/2024 11:21:05 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KMTIMER5_H_
#define KMTIMER5_H_

#ifdef KM_DOXYGEN
#define TIMSK5
#define COM5C0
#endif /* KM_DOXYGEN */

#ifdef __cplusplus
extern "C" {
#endif
#ifdef TIMSK5

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>

#include "../kmCommon/kmCommon.h"
#include "../kmTimersCommon/kmTimer5Defs.h"

/// Remapping macro for saving own user data in callback registration #timer5RegisterCallbackCompA function
#define KM_TIMER5_USER_DATA(X) (void *)(X)

/**
Definition of the Timer5 Callback Type
@param Pointer for void user data content that is used in #timer5RegisterCallbackCompA function
and will be delivered to callback."as is"
*/
typedef void kmTimer5CallbackType(void *);

/**
@brief Callback type for Timer 5 input capture events.

This is the function pointer type for the callback function that will be
called when a Timer 5 input capture event occurs.

@param[out] captureValue The captured value from the Timer 5 input capture.
@param[out] captureState The input state information obtained during the capture event.
@param[out] userData A pointer to user-defined data that was registered with the callback.

@note The callback function must adhere to this signature to be compatible
      with the Timer 5 input capture system.
*/
typedef void kmTimer5InpCaptureCallbackType(const uint16_t, const bool, const void *);

/**
Enumeration type definition of the PWM outputs for timer used in parameters of #kmTimer5SetPwmDuty, #kmTimer5SetPwmDuty, #kmTimer5SetPwmInversion functions .
*/
typedef enum {
	/// PWM on output A.
	KM_TCC5_PWM_OUT_A,
	/// PWM on output B.
	KM_TCC5_PWM_OUT_B,
#ifdef COM5C0
	/// PWM on output C.
	KM_TCC5_PWM_OUT_C
#endif
} Tcc5PwmOut;

/**
@brief Initializes Timer 5 in a mode allowing the use of CompA, CompB, CompC, and Overflow Interrupts launched at a specific phase of the timer.

The timer always counts from 0 to KM_\b Timer5_MAX value (255). Callbacks for CompA, CompB, and CompC are launched
once the timer reaches specific values set by #Timer5SetValueCompA, #Timer5SetValueCompB, and #Timer5SetValueCompC.
The callbacks need to be configured with #kmTimer5RegisterCallbackOVF, #kmTimer5RegisterCallbackCompA,
#kmTimer5RegisterCallbackCompB, and #kmTimer5RegisterCallbackCompC functions. The period/frequency of each of these Interrupts
will be exactly the same and defined by MCU main clock and the prescaler.
The Overflow callback is called when Timer 5 reaches the value \b KM_TIMER5_MAX (255).
CompA, CompB, and CompC callbacks are called when Timer 5 reaches values set by #Timer5SetValueCompA, #Timer5SetValueCompB, and #Timer5SetValueCompC respectively.
Setting these values allows shifting callback calls with phase (with accuracy limited by the time that the MCU takes to execute code within the callback).

@param prescaler Selects one of the prescalers as defined in kmTimersCommon/kmTimer5Defs.h (e.g., \b KM_TCC5_PRSC_256).

@code
	kmTimer5InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(KM_TCC0_PRSC_8);

	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer5EnableInterruptOVF();

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5SetValueCompA(KM_TIMER5_TEST_DUTY_25_PERC);
	kmTimer5EnableInterruptCompA();
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5SetValueCompB(KM_TIMER5_TEST_DUTY_50_PERC);
	kmTimer5EnableInterruptCompB();
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer5SetValueCompC(KM_TIMER5_TEST_DUTY_75_PERC);
	kmTimer5EnableInterruptCompC();
	kmTimer5ConfigureOCA(KM_TCC5_A_COMP_OUT_TOGGLE);
	kmTimer5ConfigureOCB(KM_TCC5_B_COMP_OUT_TOGGLE);
	kmTimer5ConfigureOCC(KM_TCC5_C_COMP_OUT_TOGGLE);

	kmTimer5Start();
@endcode

Output from the example code:
\image html kmTimer5Test2.png
 */
void kmTimer5InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler);

/**
Initializes Timer 5 in the mode allowing to generate frequency with specific period on OCA0 output.
This function tries to configure Timer5 to make the period as accurate as possible -
see limitations in the description of #kmTimer5CalcPerdiod function.
To change period while Timer5 is running calculate new CompA value and use #kmTimer5SetValueCompA and #kmTime0SetPrescale functions.
with kmTimer5CalcPerdiod shifting new periodInMicroseconds by one bit to the right
@param periodInMicroseconds period of the square function on the OCA0, the available range depends on the main MCU clock

Example code:
@code
	kmTimer5InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER5_TEST_1MS);
	kmTimer5Start();
@endcode
Output from the example code:
\image html kmTimer5Test0.png
*/
void kmTimer5InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds);

/**
Initializes Timer 5 in the mode allowing to use CompA Interrupt launched every specific period defined in microseconds.
The callback needs to be configured with #kmTimer5RegisterCallbackCompA function.
It's also possible to generate the square wave toggling output with the same period (output frequency is half of frequency of executing callbacks)
Note accuracy is limited with time that used by MCU to execute code within callback.
@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock

@code
	
	kmTimer5InitOnAccurateTimeCompAInterruptCallback(KM_TIMER5_TEST_5MS, true);

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();

	kmTimer5Start();
}
@endcode
Output from the example code:
\image html kmTimer5Test1.png
*/
void kmTimer5InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC5A);

/**
Initializes Timer 5 as a Counter of either rising or falling signal edges on the input T0
@param falling if true the counter will count falling edges, if false - rising edges
*/
void kmTimer5InitExternal(bool falling);

/**
Initializes Timer 5 in the mode allowing to use CompA and CompB Interrupt launched every specific period defined in microseconds.
The CompB Interrupt can be shifted in phase by providing corresponding phaseIntB parameter
The callbacks needs to be configured with #kmTimer5RegisterCallbackCompA and #kmTimer5RegisterCallbackCompB functions.
The timer goes always from 0 to top value defined as calculation for microseconds used as parameter.
This function tries to define period of calling the callbacks as accurate as possible
The range of available frequencies depends on used XTAL\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock
@param phaseIntB phase shift between executing CompA and CompB callbacks. The range of the value from 0 to 255, where 0 means no phase shift and 128 means 180 phase shift.
The accuracy of the phase shift depends on on the main MCU clock and the microseconds parameter

@code
	kmTimer5InitOnAccurateTimeCompABInterruptCallback(KM_TIMER5_TEST_1MS, KM_TIMER5_TEST_PHASE_45_DEG); // for 1 millisecond

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5EnableInterruptCompB();

	kmTimer5Start();
@endcode
Output from the example code:
\image html kmTimer5Test5.png
*/
void kmTimer5InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB);

/**
Initializes Timer 5 to generate square wave on the OC5x pin of the MCU with specified time period and duty cycle (Fast PWM mode)
To change the duty cycle after initialization #kmTimer5SetPwmDuty function can be used
(only #KM_TCC5_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC5_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 5 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC5x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC5A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC5A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC5A pin is inverted
@param dutyB duty of the square wave generated on the OC5B pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC5B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC5B pin is inverted
@param dutyC Duty of the square wave generated on the OC5C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC5C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer5InitOnAccurateTimeFastPwm(KM_TIMER5_TEST_1MS, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_25_PERC, false);

	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer5EnableInterruptOVF();
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5EnableInterruptCompB();
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer5EnableInterruptCompC();

	kmTimer5Start();
@endcode
Output from the example code:
\image html kmTimer5Test5.png
*/

#ifdef COM5C0
uint16_t kmTimer5InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedBconst, uint16_t dutyC, const bool invertedC);
#else
uint16_t kmTimer5InitOnAccurateTimeFastPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedBconst);
#endif

/**
Initializes Timer 5 to generate square wave on the OC5x pin of the MCU with specified time period and duty cycle (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer5SetPwmDuty function can be used
(only #KM_TCC5_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC5_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 5 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC5x pin of MCU, the available range and accuracy depends on the main MCU clock
@param dutyA duty of the square wave generated on the OC5A pin of the MCU; the range from 0 to MAX,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC5A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC5A pin is inverted
@param dutyB duty of the square wave generated on the OC5B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC5B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC5B pin is inverted
@param dutyC Duty of the square wave generated on the OC5C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC5C pin is inverted.

@code
	uint16_t cyclesRange = kmTimer5InitOnAccurateTimePcPwm(KM_TIMER5_TEST_1MS, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_75_PERC, false);

	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVF);
	kmTimer5EnableInterruptOVF();
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer5EnableInterruptCompA();
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer5EnableInterruptCompB();
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer5EnableInterruptCompC();

	kmTimer5Start();
@endcode
Output from the example code:
\image html kmTimer5Test7.png
*/
#ifdef COM5C0
uint16_t kmTimer5InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else
uint16_t kmTimer5InitOnAccurateTimePcPwm(const uint32_t microseconds, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif

/**
Initializes Timer 5 to generate square wave on the OC5x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer5SetPwmDuty function can be used (for both #KM_TCC5_PWM_OUT_A and #KM_TCC5_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 5 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer5Defs.h (e.g. KM_TCC5_PRSC_256)
@param dutyA duty of the square wave generated on the OC5A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC5A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC5A pin is inverted
@param dutyB duty of the square wave generated on the OC5B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC5B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC5B pin is inverted
@param dutyC Duty of the square wave generated on the OC5C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC5C pin is inverted.

@code
	kmTimer5InitOnPrescalerBottomToTopFastPwm(KM_TCC5_PRSC_1, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_75_PERC, false);

	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVFToggle);
	kmTimer5EnableInterruptOVF();
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer5EnableInterruptCompA();
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
	kmTimer5EnableInterruptCompB();
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
	kmTimer5EnableInterruptCompC();

	kmTimer5Start();
@endcode
Output from the example code:
\image html kmTimer5Test4.png
*/
#ifdef COM5C0
void kmTimer5InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else
void kmTimer5InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif

/**
Initializes Timer 5 to generate square wave on the OC5x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer5SetPwmDuty function can be used (for both #KM_TCC5_PWM_OUT_A and #KM_TCC5_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 5 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer5Defs.h (e.g. KM_TCC5_PRSC_256)
@param dutyA duty of the square wave generated on the OC5A pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC5A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC5A pin is inverted
@param dutyB duty of the square wave generated on the OC5B pin of the MCU; the range from 0 to 65535,
where 16384 means 25% and 32768 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC5B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC5B pin is inverted
@param dutyC Duty of the square wave generated on the OC5C pin of the MCU; the range from 0 to 65535, where 16384 means 25% and 32768 means 50% duty cycle;
the accuracy of the value depends on the main clock of the MCU and microseconds parameter, the function calculates it in the way to be at least equal or higher than
#KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig.
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY.
@param invertedC If true, the output of the square wave generated on OC5C pin is inverted.

@code
	kmTimer5InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_1, KM_TIMER5_TEST_DUTY_25_PERC, false, KM_TIMER5_TEST_DUTY_50_PERC, false, KM_TIMER5_TEST_DUTY_75_PERC, false);

	kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_D), callbackOVF);
	kmTimer5EnableInterruptOVF();
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer5EnableInterruptCompA();
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBOff);
	kmTimer5EnableInterruptCompB();
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCOff);
	kmTimer5EnableInterruptCompC();

	kmTimer5Start();
@endcode
Output from the example code:
\image html kmTimer5Test6.png
*/
#ifdef COM5C0
void kmTimer5InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB, const uint16_t dutyC, const bool invertedC);
#else
void kmTimer5InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint16_t dutyA, const bool invertedA, const uint16_t dutyB, const bool invertedB);
#endif

/**
@brief Initializes Timer/Counter 5 for input capture with specified parameters.

This function initializes Timer/Counter 5 for input capture mode, configuring it
with the specified parameters. It sets up the necessary callbacks, enables interrupts,
and starts the timer for accurate timing measurements.
The iddle time defines maximum time of captured input state change.

@param iddleTimeMicroseconds The idle time (in microseconds) before capturing signals.
@param iddleUserData User data to be passed to the idle time callback function.
@param iddleCallback Pointer to the callback function for the idle time interrupt.
@param inpCaptureUserData User data to be passed to the input capture callback function.
@param inpCaptureCallback Pointer to the callback function for the input capture interrupt.
 */
void kmTimer5InitInputCapture(const uint32_t iddleTimeMicroseconds,
							void *iddleUserData, kmTimer5CallbackType *iddleCallback,
							void *inpCaptureUserData, kmTimer5InpCaptureCallbackType *inpCaptureCallback);

// Setters callback user data
/**
Sets or changes User Data passed to the callback registered with #kmTimer5RegisterCallbackOVF.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer5SetCallbackUserDataOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C));
@endcode
*/
void kmTimer5SetCallbackUserDataOVF(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer5RegisterCallbackCompA.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A));
@endcode
*/
void kmTimer5SetCallbackUserDataCompA(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer5RegisterCallbackCompB.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B));
@endcode
*/
void kmTimer5SetCallbackUserDataCompB(void *userData);

#ifdef COM5C0
/**
Sets or changes User Data passed to the callback registered with #kmTimer5RegisterCallbackCompC.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer5RegisterCallbackCompC(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B));
@endcode
*/
void kmTimer5SetCallbackUserDataCompC(void *userData);
#endif

// Registering callbacks
/**
Functions to register callback launched on Timer5 Overflow. The callback needs to be declared with #kmTimer5CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer5CallbackType type

@code
void callbackOVFToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackOVFToggle);
@endcode
*/
void kmTimer5RegisterCallbackOVF(void *userData, kmTimer5CallbackType *callback);

/**
Functions to register callback launched on Timer5 Overflow Interrupt. The callback needs to be declared with #kmTimer5CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer5CallbackType type

@code
void callbackCompAToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAToggle);
@endcode
*/
void kmTimer5RegisterCallbackCompA(void *userData, kmTimer5CallbackType *callback);

/**
Functions to register callback launched on Timer5 Overflow Interrupt. The callback needs to be declared with #kmTimer5CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer5CallbackType type

@code
void callbackCompBToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_1);
}

/// application routine
kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackCompBToggle);
@endcode
*/
void kmTimer5RegisterCallbackCompB(void *userData, kmTimer5CallbackType *callback);

#ifdef COM5C0
/**
Functions to register callback launched on Timer5 Overflow Interrupt. The callback needs to be declared with #kmTimer5CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER5_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer5CallbackType type

@code
void callbackCompCToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_2);
}

/// application routine
kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompCToggle);
@endcode
*/
void kmTimer5RegisterCallbackCompC(void *userData, kmTimer5CallbackType *callback);
#endif

/**
@brief Register a callback function for Timer 5 input capture event.

This function registers a user-provided callback function that will be
called when a Timer 5 input capture event occurs. The user can also
provide a custom data pointer which will be passed back to the callback
when it is invoked.

@param[in] userData A pointer to user-defined data that will be passed to
                    the callback function.
@param[in] callback A pointer to the function that will be called on Timer 5
                    input capture event. This function must match the
                    signature defined by kmTimer5InpCaptureCallbackType.

@note The callback function must be set up before enabling the Timer 5 input
      capture to ensure that events are handled correctly.
*/
void kmTimer5RegisterCallbackInpCapture(void *userData, kmTimer5InpCaptureCallbackType *callback);

// Unregistering callbacks
/**
Permanently unregisters callback registered with #kmTimer5RegisterCallbackOVF function
*/
void kmTimer5UnregisterCallbackOVF(void);

/**
Permanently unregisters callback registered with #kmTimer5RegisterCallbackCompA function
*/
void kmTimer5UnregisterCallbackCompA(void);

/**
Permanently unregisters callback registered with #kmTimer5RegisterCallbackCompB function.
*/
void kmTimer5UnregisterCallbackCompB(void);

#ifdef COM5C0
/**
Permanently unregisters callback registered with #kmTimer5RegisterCallbackCompC function.
*/
void kmTimer5UnregisterCallbackCompC(void);
#endif

/**
Permanently unregisters callback registered with #kmTimer5RegisterCallbackInpCapture function.
*/
void kmTimer5UnregisterCallbackInpCapture(void);

// Enabling interrupts
/**
Enables Timer5 Overflow interrupt allowing to execute routine registered with #kmTimer5RegisterCallbackOVF
*/
void kmTimer5EnableInterruptOVF(void);

/**
Enables Timer5 Comparator A interrupt allowing to execute routine registered with #kmTimer5RegisterCallbackCompA
*/
void kmTimer5EnableInterruptCompA(void);

/**
Enables Timer5 Comparator B interrupt allowing to execute routine registered with #kmTimer5RegisterCallbackCompB
*/
void kmTimer5EnableInterruptCompB(void);

#ifdef COM5C0
/**
Enables Timer5 Comparator C interrupt allowing to execute routine registered with #kmTimer5RegisterCallbackCompC
*/
void kmTimer5EnableInterruptCompC(void);
#endif

/**
@brief Enables Timer/Counter 5 Input Capture Interrupt.

This function sets the Input Capture Interrupt Enable bit for Timer/Counter 5.
After calling this function, the timer will generate an interrupt when an input capture event occurs.
The specific input capture event conditions are determined by the timer configuration and mode.
Ensure that the corresponding input capture callback is registered using #kmTimer5RegisterCallbackInpCapture
before enabling the input capture interrupt.

Example usage:
@code
kmTimer5EnableInterruptInpCapture();
@endcode
 */
void kmTimer5EnableInterruptInpCapture(void);

// Disabling interrupts
/**
Disables all Timer 5 interrupts (Overflow, ComparatorA, ComparatorB, ComparatorC & Input Capture).
*/
void kmTimer5DisableInterruptsAll(void);

/**
Disables Timer 5 Overflow interrupt
*/
void kmTimer5DisableInterruptOVF(void);

/**
Disables Timer 5 ComparatorA interrupt
*/
void kmTimer5DisableInterruptCompA(void);

/**
Disables Timer 5 ComparatorB interrupt
*/
void kmTimer5DisableInterruptCompB(void);

#ifdef COM5C0
/**
Disables Timer 5 ComparatorC interrupt
*/
void kmTimer5DisableInterruptCompC(void);
#endif

/**
@brief Sets the value for Timer/Counter 5 Overflow.

This function sets the value for Timer/Counter 5 Overflow. The timer counts from 0 to this value
before generating an overflow interrupt and resetting to zero. Ensure that the corresponding
overflow callback is registered using #kmTimer5RegisterCallbackOVF before setting the overflow value.

@param value The value to set for Timer/Counter 5 Overflow.

Example usage:
@code
kmTimer5SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
@endcode
 */
void kmTimer5SetValueOvf(uint16_t value);

// Configuration of Comparator Outputs
/**
Configures Compare Output Mode channel A for Timer5 (\b OC5A output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer5Defs.h (e.g. \b KM_TCC5_A_COMP_OUT_TOGGLE)
*/
void kmTimer5ConfigureOCA(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel B for Timer5 (\b OC5B output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer5Defs.h (e.g. \b KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer5ConfigureOCB(uint8_t compareOutputMode);

/**
Configures Compare Output Mode channel C for Timer5 (\b OC5C output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in
kmTimersCommon/kmTimer5Defs.h (e.g. \b KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer5ConfigureOCC(uint8_t compareOutputMode);

// Controlling timer flow
/**
Enables running of Timer 5, allowing to execute Timer/Counter function defined with Initialization functions
*/
void kmTimer5Start(void);

/**
Stops running of Timer 5
*/
void kmTimer5Stop(void);

/**
Restarts internal counter of Timer 5.
*/
void kmTimer5Restart(void);

// Controlling PWM
/**
Sets the Duty in the PWM modes initialized as Bottom-To-Top. Can be applied to both OC5A and OC5B outputs
@param pwmOut either KM_TCC5_PWM_OUT_A for \b OC5A or KM_TCC5_PWM_OUT_B for \b OC5B
@param duty duty of the square wave generated on the OC5A/OC5B pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle
*/

void kmTimer5SetPwmDutyBottomToTop(Tcc5PwmOut pwmOut, uint16_t duty);
/**
Sets the Duty in the PWM modes initialized as Accurate-Time. Can be applied to OC5B output only (as Comparator A is used to control general time period)
@param duty duty of the square wave generated on the OC5A/OC5B pin of the MCU; the range from 0 to 255,
where 64 means 25%/75% and 128 means 50%/50% duty cycle; this initialization offers maximum accuracy of the duty cycle
The accuracy of the value depends on the main clock of the MCU and \e microseconds parameter provided in initialization function
The function calculates it in the way to be at least equal or higher than #KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer5DefaultConfig
*/
void kmTimer5SetPwmDutyAccurateTimeModes(uint16_t duty);

/**
Function allows to invert PWM output either on \b OC5A or on \b OC5B
@param pwmOut either KM_TCC5_PWM_OUT_A for \b OC5A or KM_TCC5_PWM_OUT_B for \b B
@param inverted value true inverts the PWM output, normal operation when falseOC3
*/
void kmTimer5SetPwmInversion(Tcc5PwmOut pwmOut, bool inverted);

// Timer flow calculations
/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period\n

@param microseconds time period of the expected Comparator matches for Timer 5
the available range and accuracy depends on the main MCU clock as specified in the table below
\b NOTE: Some modes of Timer5 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.\n
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer5Defs.h to be used in #kmTimer5Init
@return comparator match value to be used in Timer initialization #kmTimer5Init function
*/
uint16_t kmTimer5CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);

/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period. This version takes into consideration minim resolution of PWM duty and decreases frequency if needed\n
@param microseconds time period of the expected Comparator matches for Timer 5
the available range and accuracy depends on the main MCU clock as specified in the table below.
Note this version of the function takes into consideration desired minimum resolution of the output square wave duty.
In practice this may only matter for very low main clock frequencies. If the desired minimum resolution cannot be reached,
this function will return prescaler and comparator match value decreasing desired period defined in milliseconds
\b NOTE: Some modes of Timer5 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer5Defs.h to be used in #kmTimer5Init
@param minimumCycleAcuracy desired minimum resolution of the output square wave duty (e.g. 4 means that at least 0%, 25%, 50% and 75% are achievable)
@return comparator match value to be used in Timer initialization #kmTimer5Init function
*/
uint16_t kmTimer5CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);

/**
Calculates compare match result based on desired square wave duty and available compare match cycles
calculated for Accurate-Time modes (e.g. got as result of #kmTimer5CalcPerdiod or #kmTimer5InitOnAccurateTimeFastPwm)
@param duty desired duty cycle
@param cyclesRange available range of cycles for duty cycle
@return comparator match value corresponding to desired duty cycle (can be used e.g. in #kmTimer5SetValueCompA)
*/
uint16_t kmTimer5CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange);


/**
@brief Retrieves the current value of Timer/Counter 5 Overflow.

This function returns the current value of Timer/Counter 5 Overflow, representing the number of cycles
before the timer overflows and triggers an overflow interrupt.

@return The current value of Timer/Counter 5 Overflow.

Example usage:
@code
uint16_t ovfValue = kmTimer5GetValueOvf();
@endcode
 */
uint16_t kmTimer5GetValueOvf(void);

/**
@brief Retrieves the current value of Timer/Counter 5 Compare A.

This function returns the current value of Timer/Counter 5 Compare A, representing the number of cycles
before the timer triggers a Compare A interrupt.

@return The current value of Timer/Counter 5 Compare A.

Example usage:
@code
uint16_t compAValue = kmTimer5GetValueCompA();
@endcode
 */
uint16_t kmTimer5GetValueCompA(void);

/**
@brief Retrieves the current value of Timer/Counter 5 Compare B.

This function returns the current value of Timer/Counter 5 Compare B, representing the number of cycles
before the timer triggers a Compare B interrupt.

@return The current value of Timer/Counter 5 Compare B.

Example usage:
@code
uint16_t compBValue = kmTimer5GetValueCompB();
@endcode
 */
uint16_t kmTimer5GetValueCompB(void);

#ifdef COM5C0
/**
@brief Retrieves the current value of Timer/Counter 5 Compare C.

This function returns the current value of Timer/Counter 5 Compare C, representing the number of cycles
before the timer triggers a Compare C interrupt.

@return The current value of Timer/Counter 5 Compare C.

Example usage:
@code
uint16_t compCValue = kmTimer5GetValueCompC();
@endcode
 */
uint16_t kmTimer5GetValueCompC(void);
#endif

/**
@brief Sets the value for Timer/Counter 5 Overflow.

This function sets the value for Timer/Counter 5 Overflow. The timer counts from 0 to this value
before generating an overflow interrupt and resetting to zero. The value is also stored internally
for future reference.

@param value The value to set for Timer/Counter 5 Overflow.

Example usage:
@code
kmTimer5SetValueOvf(65535); // Set overflow value to the maximum (16-bit)
@endcode
 */
void kmTimer5SetValueOvf(uint16_t value);

/**
@brief Sets the value for Timer/Counter 5 Compare A.

This function sets the value for Timer/Counter 5 Compare A. The timer counts from 0 to this value
before triggering a Compare A interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 5 Compare A.

Example usage:
@code
kmTimer5SetValueCompA(5000); // Set Compare A value to 5000
@endcode
 */
void kmTimer5SetValueCompA(uint16_t value);

/**
@brief Sets the value for Timer/Counter 5 Compare B.

This function sets the value for Timer/Counter 5 Compare B. The timer counts from 0 to this value
before triggering a Compare B interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 5 Compare B.

Example usage:
@code
kmTimer5SetValueCompB(3000); // Set Compare B value to 3000
@endcode
 */
void kmTimer5SetValueCompB(uint16_t value);

#ifdef COM5C0
/**
@brief Sets the value for Timer/Counter 5 Compare C.

This function sets the value for Timer/Counter 5 Compare C. The timer counts from 0 to this value
before triggering a Compare C interrupt. The value is also stored internally for future reference.

@param value The value to set for Timer/Counter 5 Compare C.

Example usage:
@code
kmTimer5SetValueCompC(1000); // Set Compare C value to 1000
@endcode
 */
void kmTimer5SetValueCompC(uint16_t value);
#endif

/**
@brief Gets the state of Timer/Counter 5 Input Capture pin.

This function returns the current state of the Timer/Counter 5 Input Capture pin.
It checks whether the Input Capture pin is currently high or low.

@return The state of the Timer/Counter 5 Input Capture pin:
        - \c true if the Input Capture pin is high.
        - \c false if the Input Capture pin is low.

Example usage:
@code
bool inpCaptureState = kmTimer5InpCaputureGetState();
if (inpCaptureState) {
    // Input Capture pin is high
} else {
    // Input Capture pin is low
}
@endcode
 */
bool kmTimer5InpCaputureGetState(void);

/**
@brief Initializes Timer 5 with the specified prescaler and modes.

This function initializes Timer 5 with the specified prescaler and modes.

@param prescaler The prescaler value to be set for Timer 5.
@param modeA The mode value to be set for Timer 5 control register A (TCCR5A).
@param modeB The mode value to be set for Timer 5 control register B (TCCR5B).

@note This function stops Timer 5 before initializing it.

@see kmTimer5Stop()
 */
void kmTimer5Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);

/**
@brief Starts Timer 5 with the previously configured prescaler.

This function starts Timer 5 with the previously configured prescaler.
 */
void kmTimer5Start(void);

/**
@brief Stops Timer 5.

This function stops Timer 5.
 */
void kmTimer5Stop(void);

/**
@brief Restarts Timer 5 by resetting its counter value to zero.

This function restarts Timer 5 by resetting its counter value to zero.
 */
void kmTimer5Restart(void);

#endif /* TIMSK5 */

#ifdef __cplusplus
}
#endif /*  __cplusplus */
#endif /* KMTIMER5_H_ */

