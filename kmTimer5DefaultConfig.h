/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file kmTimer5DefaultConfig.h
*
*  **Created on**: 3/15/2024 11:21:39 PM @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Copyright (C) 2024 Krzysztof Moskwa
*  kmTimer5DefaultConfig.h
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef KMTIMER5DEFAULTCONFIG_H_
#define KMTIMER5DEFAULTCONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "../kmCommon/kmCommon.h"

/// Defines minimum possible steps for accurate time PWM timer functions. In case less value is calculated, this will be used
#define KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY 4


#ifdef __cplusplus
}
#endif
#endif /* KMTIMER5DEFAULTCONFIG_H_ */

