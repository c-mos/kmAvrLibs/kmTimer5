/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//* @file kmTimer5.c
*
*  Created on: 3/15/2024 11:21:16 PM
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Test Application for kmAvrLibs
*  Copyright (C) 2024  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "../kmCommon/kmCommon.h"

#include "kmTimer5.h"

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/atomic.h>

#include "../kmTimersCommon/kmTimer5Defs.h"

#ifdef TIMSK5

// Definitions
#define KM_TCC5_TOP KM_TCC_TOP_3

#define KM_TCC5_CYCLES_CALCULATION_CORRECTION 1u

// "Private" variables
static uint8_t _timer5PrescalerSelectBits = 0;

static uint16_t _timer5CompOvfCycles = KM_TCC5_TOP;

static uint16_t _timer5CompACycles = KM_TCC5_TOP;
static bool _Timer5OtputComparePinUsedA = false;

static uint16_t _timer5CompBCycles = KM_TCC5_TOP;
static bool _Timer5OtputComparePinUsedB = false;

#ifdef OCR5C
static uint16_t _timer5CompCCycles = KM_TCC5_TOP;
static bool _Timer5OtputComparePinUsedC = false;
#endif

static kmTimer5CallbackType *_timer5CallbackOVF = NULL;
static void *_timer5CallbackUserDataOVF = NULL;

static kmTimer5CallbackType *_timer5CallbackCompA = NULL;
static void *_Timer5CallbackUserDataCompA = NULL;

static kmTimer5CallbackType *_timer5CallbackCompB = NULL;
static void *_Timer5CallbackUserDataCompB = NULL;

#ifdef OCR5C
static kmTimer5CallbackType *_timer5CallbackCompC = NULL;
static void *_Timer5CallbackUserDataCompC = NULL;
#endif

static kmTimer5InpCaptureCallbackType *_timer5CallbackInpCapture = NULL;
static void *_Timer5CallbackUserDataInpCapture = NULL;
static uint16_t _inpCaptureCycles = 0;

// "Private" functions

// "Public" functions
// Initializations
void kmTimer5Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);
void kmTimer5InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler);
void kmTimer5InitOnAccuratePeriodGenerateOutputClockA(const uint32_t period);
void kmTimer5InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC5A);
void kmTimer5InitExternal(bool falling);

void kmTimer5InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB);
// Setters callback user data
void kmTimer5SetCallbackUserDataOVF(void *userData);
void kmTimer5SetCallbackUserDataCompA(void *userData);
void kmTimer5SetCallbackUserDataCompB(void *userData);
#ifdef OCR5C
void kmTimer5SetCallbackUserDataCompC(void *userData);
#endif

// Registering callbacks
void kmTimer5RegisterCallbackOVF(void *userData, void (*callback)(void *));
void kmTimer5RegisterCallbackCompA(void *userData, void (*callback)(void *));
void kmTimer5RegisterCallbackCompB(void *userData, void (*callback)(void *));
#ifdef OCR5C
void kmTimer5RegisterCallbackCompC(void *userData, void (*callback)(void *));
#endif

// Unregistering callbacks
void kmTimer5UnregisterCallbackOVF(void);
void kmTimer5UnregisterCallbackCompA(void);
void kmTimer5UnregisterCallbackCompB(void);
#ifdef OCR5C
void kmTimer5UnregisterCallbackCompC(void);
#endif

// Enabling interrupts
void kmTimer5EnableInterruptOVF(void);
void kmTimer5EnableInterruptCompA(void);
void kmTimer5EnableInterruptCompB(void);
#ifdef OCR5C
void kmTimer5EnableInterruptCompC(void);
#endif

// Disabling interrupts
void kmTimer5DisableInterruptsAll(void);
void kmTimer5DisableInterruptOVF(void);
void kmTimer5DisableInterruptCompA(void);
void kmTimer5DisableInterruptCompB(void);
#ifdef OCR5C
void kmTimer5DisableInterruptCompC(void);
#endif

// Configuration of Comparator Outputs
void kmTimer5ConfigureOCA(uint8_t compareOutputMode);
void kmTimer5ConfigureOCB(uint8_t compareOutputMode);
#ifdef OCR5C
void kmTimer5ConfigureOCC(uint8_t compareOutputMode);
#endif

// Controlling timer flow
void kmTimer5Start(void);
void kmTimer5Stop(void);
void kmTimer5Restart(void);

// Controlling PWM
void kmTimer5SetPwmDutyBottomToTop(Tcc5PwmOut pwmOut, uint16_t duty);
void kmTimer5SetPwmInversion(Tcc5PwmOut pwmOut, bool inverted);

// Timer flow calculations
uint16_t kmTimer5CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);
uint16_t kmTimer5CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy);
uint16_t kmTimer5CalcDutyOnCycles(uint16_t duty, uint16_t cycles);

// Implementation
void kmTimer5Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB) {
	kmTimer5Stop();
	_timer5PrescalerSelectBits = prescaler;
	TCCR5A = (TCCR5A & ~KM_TCC5_MODE_MASK_A) | modeA;
	TCCR5B = (TCCR5B & ~KM_TCC5_MODE_MASK_B) | modeB;
}

void kmTimer5InitOnPrescalerBottomToTopOvfCompABCInterruptCallback(const uint8_t prescaler) {
	kmTimer5Init(prescaler, KM_TCC5_MODE_0_A, KM_TCC5_MODE_0_B);
}

void kmTimer5InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds) {
	kmTimer5InitOnAccurateTimeCompAInterruptCallback(periodInMicroseconds >> KMC_DIV_BY_2, true);
}

void kmTimer5InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC5A) {
	_timer5CompACycles = kmTimer5CalcPerdiod(microseconds, &_timer5PrescalerSelectBits);
	kmTimer5Init(_timer5PrescalerSelectBits, KM_TCC5_MODE_4_A, KM_TCC5_MODE_4_B);
	kmTimer5SetValueCompA(_timer5CompACycles);
	kmTimer5ConfigureOCA(outOnOC5A ? KM_TCC5_A_PWM_COMP_OUT_TOGGLE : KM_TCC5_A_PWM_COMP_OUT_NORMAL);
}

void kmTimer5InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint16_t phaseIntB) {
	_timer5CompACycles = kmTimer5CalcPerdiod(microseconds, &_timer5PrescalerSelectBits);
	_timer5CompBCycles = kmTimer5CalcDutyOnCycles(phaseIntB, _timer5CompACycles);
	kmTimer5Init(_timer5PrescalerSelectBits, KM_TCC5_MODE_4_A, KM_TCC5_MODE_4_B);
	kmTimer5SetValueCompA(_timer5CompACycles);
	kmTimer5SetValueCompB(_timer5CompBCycles);
}

void kmTimer5InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer5Init(prescaler, KM_TCC5_MODE_E_A, KM_TCC5_MODE_E_B);
	kmTimer5SetValueOvf(_timer5CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		kmTimer5ConfigureOCA(invertedA ? KM_TCC5_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer5SetValueCompA(dutyA);
	}
}

void kmTimer5InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA
												, const uint16_t dutyB
												, const bool invertedB
#ifdef OCR5C
												, const uint16_t dutyC
												, const bool invertedC
#endif
												) {
	kmTimer5InitOnPrescalerBottomToTopFastPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > KMC_UNSIGNED_ZERO) {
		kmTimer5ConfigureOCB(invertedB ? KM_TCC5_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer5SetValueCompB(dutyB);
	}

#ifdef OCR5C
	if (dutyC > KMC_UNSIGNED_ZERO) {
		kmTimer5ConfigureOCC(invertedC ? KM_TCC5_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer5SetValueCompC(dutyC);
	}
#endif
}

uint16_t kmTimer5InitOnAccurateTimeFastPwm(const uint32_t microseconds
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef OCR5C
											, const uint16_t dutyC
											, const bool invertedC
#endif
											) {
	
	_timer5CompOvfCycles = kmTimer5CalcPerdiodWithMinPwmAccuracy(microseconds, &_timer5PrescalerSelectBits, KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer5Init(_timer5PrescalerSelectBits, KM_TCC5_MODE_E_A, KM_TCC5_MODE_E_B);
	kmTimer5SetValueOvf(_timer5CompOvfCycles);

	if (dutyA > KMC_UNSIGNED_ZERO) {
		_timer5CompACycles = kmTimer5CalcDutyOnCycles(dutyA, _timer5CompOvfCycles);
		kmTimer5SetValueCompA(_timer5CompACycles);
		kmTimer5ConfigureOCA(invertedA ? KM_TCC5_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_timer5CompBCycles = kmTimer5CalcDutyOnCycles(dutyB, _timer5CompOvfCycles);
		kmTimer5SetValueCompB(_timer5CompBCycles);
		kmTimer5ConfigureOCB(invertedB ? KM_TCC5_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#ifdef OCR5C
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_timer5CompCCycles = kmTimer5CalcDutyOnCycles(dutyC, _timer5CompOvfCycles);
		kmTimer5SetValueCompC(_timer5CompCCycles);
		kmTimer5ConfigureOCC(invertedC ? KM_TCC5_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif

	return _timer5CompOvfCycles;
}

void kmTimer5InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler
												, const uint16_t dutyA
												, const bool invertedA) {
	kmTimer5Init(prescaler, KM_TCC5_MODE_A_A, KM_TCC5_MODE_A_B);
	kmTimer5SetValueOvf(_timer5CompOvfCycles);

	kmTimer5SetValueCompA(dutyA);
	kmTimer5ConfigureOCA(invertedA ? KM_TCC5_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
}

void kmTimer5InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler
											, const uint16_t dutyA
											, const bool invertedA
											, const uint16_t dutyB
											, const bool invertedB
#ifdef OCR5C
											, const uint16_t dutyC
											, const bool invertedC
#endif
											) {
	kmTimer5InitOnPrescalerBottomToTopPcPwmOCA(prescaler, dutyA, invertedA);

	if (dutyB > 0) {
		kmTimer5SetValueCompB(dutyB);
		kmTimer5ConfigureOCB(invertedB ? KM_TCC5_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef OCR5C
	if (dutyC > 0) {
		kmTimer5SetValueCompC(dutyC);
		kmTimer5ConfigureOCC(invertedC ? KM_TCC5_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif
}

uint16_t kmTimer5InitOnAccurateTimePcPwm(const uint32_t microseconds
										, const uint16_t dutyA
										, const bool invertedA
										, const uint16_t dutyB
										, const bool invertedB
#ifdef OCR5C
										, const uint16_t dutyC
										, const bool invertedC
#endif
										) {
	_timer5CompOvfCycles = kmTimer5CalcPerdiodWithMinPwmAccuracy(microseconds >> KMC_DIV_BY_2, &_timer5PrescalerSelectBits, KM_TCC5_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer5SetValueOvf(_timer5CompOvfCycles);
	kmTimer5Init(_timer5PrescalerSelectBits, KM_TCC5_MODE_A_A, KM_TCC5_MODE_A_B);
	
	if (dutyA > KMC_UNSIGNED_ZERO) {
		_timer5CompACycles = kmTimer5CalcDutyOnCycles(dutyA, _timer5CompOvfCycles);
		kmTimer5SetValueCompA(_timer5CompACycles);
		kmTimer5ConfigureOCA(invertedB ? KM_TCC5_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_timer5CompBCycles = kmTimer5CalcDutyOnCycles(dutyB, _timer5CompOvfCycles);
		kmTimer5SetValueCompB(_timer5CompBCycles);
		kmTimer5ConfigureOCB(invertedB ? KM_TCC5_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}

#ifdef OCR5C
	if (dutyC > KMC_UNSIGNED_ZERO) {
		_timer5CompCCycles = kmTimer5CalcDutyOnCycles(dutyC, _timer5CompOvfCycles);
		kmTimer5SetValueCompC(_timer5CompCCycles);
		kmTimer5ConfigureOCC(invertedC ? KM_TCC5_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
#endif

	return _timer5CompOvfCycles;
}

void kmTimer5InitExternal(bool falling) {
	if (true == falling) {
		_timer5PrescalerSelectBits = KM_TCC5_EXT_T5_FAL;
		} else {
		_timer5PrescalerSelectBits = KM_TCC5_EXT_T5_RIS;
	}
}

void kmTimer5InitInputCapture(const uint32_t iddleTimeMicroseconds,
				void *iddleUserData, kmTimer5CallbackType *iddleCallback,
				void *inpCaptureUserData, kmTimer5InpCaptureCallbackType *inpCaptureCallback) {
	KM_TCC5_ICP5_PORT_DDR &= ~_BV(KM_TCC5_ICP5_PIN);
	KM_TCC5_ICP5_PORT_OUTPUT |= _BV(KM_TCC5_ICP5_PIN);

	kmTimer5InitOnAccurateTimeCompAInterruptCallback(iddleTimeMicroseconds, false);

	kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(iddleUserData), iddleCallback);
	kmTimer5RegisterCallbackInpCapture(KM_TIMER5_USER_DATA(inpCaptureUserData), inpCaptureCallback);
	kmTimer5EnableInterruptCompA();
	kmTimer5EnableInterruptInpCapture();

	kmTimer5Start();
}

uint16_t kmTimer5CalcDutyOnCycles(uint16_t duty, uint16_t cyclesRange) {
	uint32_t result = ((uint32_t)cyclesRange * (uint32_t)duty) / (uint32_t)KM_TCC5_TOP;
	return (uint16_t)result;
}

void kmTimer5SetPwmDutyAccurateTimeModes(uint16_t duty) {
	kmTimer5SetValueCompB(kmTimer5CalcDutyOnCycles(duty, _timer5CompACycles));
}

void kmTimer5SetPwmDutyBottomToTop(Tcc5PwmOut pwmOut, uint16_t duty) {
	switch (pwmOut) {
		case KM_TCC5_PWM_OUT_A: {
			kmTimer5SetValueCompA(duty);
			break;
		}
#ifdef OCR5B
		case KM_TCC5_PWM_OUT_B: {
			kmTimer5SetValueCompB(duty);
			break;
		}
#endif
#ifdef OCR5C
		case KM_TCC5_PWM_OUT_C: {
			kmTimer5SetValueCompC(duty);
			break;
		}
#endif
		// no default
	}
}

uint16_t kmTimer5CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler) {
	uint64_t cycles = (int64_t)(F_CPU);
	cycles *= microseconds;
	cycles /= KMC_CONV_MICROSECONDS_TO_SECONCS;
	if (cycles <= KM_TCC5_TOP) {
		// no prescaler, full XTAL
		*prescaler = KM_TCC5_PRSC_1;
		} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC5_TOP) {
		// prescaler by /8
		*prescaler = KM_TCC5_PRSC_8;
		} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC5_TOP) {
		// prescaler by /64
		*prescaler = KM_TCC5_PRSC_64;
		} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC5_TOP) {
		// prescaler by /256
		*prescaler = KM_TCC5_PRSC_256;
		} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC5_TOP) {
		// prescaler by /1024
		*prescaler = KM_TCC5_PRSC_1024;
		} else {
		// request was out of bounds, set as maximum
		*prescaler = KM_TCC5_PRSC_1024;
		cycles = KM_TCC5_TOP;
	}
	return (uint16_t)(cycles != KMC_UNSIGNED_ZERO ? cycles - KM_TCC5_CYCLES_CALCULATION_CORRECTION : cycles);
}

uint16_t kmTimer5CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint16_t minimumCycleAcuracy) {
	uint16_t result = kmTimer5CalcPerdiod(microseconds, prescaler);
	
	if (result < minimumCycleAcuracy) {
		result = minimumCycleAcuracy;
	}
	
	return result;
}

void kmTimer5SetPwmInversion(Tcc5PwmOut pwmOut, bool inverted) {
	switch (pwmOut) {
		case KM_TCC5_PWM_OUT_A: {
			if (false == inverted) {
				kmTimer5ConfigureOCA(KM_TCC5_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer5ConfigureOCA(KM_TCC5_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
		case KM_TCC5_PWM_OUT_B: {
			if (false == inverted) {
				kmTimer5ConfigureOCB(KM_TCC5_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer5ConfigureOCB(KM_TCC5_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#ifdef OCR5C
		case KM_TCC5_PWM_OUT_C: {
			if (false == inverted) {
				kmTimer5ConfigureOCC(KM_TCC5_C_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer5ConfigureOCC(KM_TCC5_C_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#endif
		default: {
			// intentionally
		}
	}
}

void kmTimer5Start(void) {
	TCCR5B = (TCCR5B & ~KM_TCC5_CS_MASK) | _timer5PrescalerSelectBits;
}

void kmTimer5Stop(void) {
	TCCR5B &= (TCCR5B & ~KM_TCC5_CS_MASK) | KM_TCC5_STOP;
}

void kmTimer5Restart(void) {
	TCNT5 = KMC_UNSIGNED_ZERO;
}

void kmTimer5ConfigureOCA(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC5_A_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _Timer5OtputComparePinUsedA) {
		return;
	}
	
	KM_TCC5_PWM_DDR |= KM_TCC5_PWM_BV_A;		// setup port for output
	KM_TCC5_PWM_PORT |= KM_TCC5_PWM_BV_A;		// HIGH by default

	TCCR5A = (TCCR5A & ~KM_TCC5_COMP_A_MASK) | compareOutputMode;

	_Timer5OtputComparePinUsedA = true;
}

void kmTimer5ConfigureOCB(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC5_B_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _Timer5OtputComparePinUsedB) {
		return;
	}
	KM_TCC5_PWM_DDR |= KM_TCC5_PWM_BV_B;		// setup port for output
	KM_TCC5_PWM_PORT |= KM_TCC5_PWM_BV_B;		// HIGH by default

	TCCR5A = (TCCR5A & ~KM_TCC5_COMP_B_MASK) | compareOutputMode;

	_Timer5OtputComparePinUsedB = true;
}

#ifdef OCR5C
void kmTimer5ConfigureOCC(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC5_C_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _Timer5OtputComparePinUsedC) {
		return;
	}
	KM_TCC5_PWM_DDR |= KM_TCC5_PWM_BV_C;		// setup port for output
	KM_TCC5_PWM_PORT |= KM_TCC5_PWM_BV_C;		// HIGH by default

	TCCR5A = (TCCR5A & ~KM_TCC5_COMP_C_MASK) | compareOutputMode;

	_Timer5OtputComparePinUsedC = true;
}
#endif

void kmTimer5SetValueOvf(uint16_t value) {
	ICR5 = _timer5CompACycles = value; // -V2561
}

void kmTimer5SetValueCompA(uint16_t value) {
	OCR5A = _timer5CompACycles = value; // -V2561
}

void kmTimer5SetValueCompB(uint16_t value) {
	OCR5B = _timer5CompBCycles = value; // -V2561
}

#ifdef OCR5C
void kmTimer5SetValueCompC(uint16_t value) {
	OCR5C = _timer5CompCCycles = value; // -V2561
}
#endif

uint16_t kmTimer5GetValueOvf(void) {
	return _timer5CompOvfCycles;
}

uint16_t kmTimer5GetValueCompA(void) {
	return _timer5CompACycles;
}

uint16_t kmTimer5GetValueCompB(void) {
	return _timer5CompBCycles;
}

#ifdef OCR5C
uint16_t kmTimer5GetValueCompC(void) {
	return _timer5CompCCycles;
}
#endif

void kmTimer5SetPrescale(uint8_t prescaler) {
	_timer5PrescalerSelectBits = prescaler;
	kmTimer5Start();
}

void kmTimer5EnableInterruptCompA(void) {
	TIMSK5 |= _BV(OCIE5A);
}

void kmTimer5EnableInterruptCompB(void) {
	TIMSK5 |= _BV(OCIE5B);
}

#ifdef OCR5C
void kmTimer5EnableInterruptCompC(void) {
	TIMSK5 |= _BV(OCIE5C);
}
#endif

void kmTimer5EnableInterruptOVF(void) {
	TIMSK5 |= _BV(TOIE5);
}

void kmTimer5EnableInterruptInpCapture(void) {
	TIMSK5 |= _BV(ICIE5);
}

void kmTimer5DisableInterruptCompA(void) {
	TIMSK5 &= ~_BV(OCIE5A);
}

void kmTimer5DisableInterruptCompB(void) {
	TIMSK5 &= ~_BV(OCIE5B);
}

#ifdef OCR5C
void kmTimer5DisableInterruptCompC(void) {
	TIMSK5 &= ~_BV(OCIE5C);
}
#endif

void kmTimer5DisableInterruptOVF(void) {
	TIMSK5 &= ~_BV(TOIE5);
}

void kmTimer5DisableInterruptInpCapture(void) {
	TIMSK5 &= ~_BV(ICIE5);
}

void kmTimer5DisableInterruptsAll(void) {
#ifdef OCR5C
	TIMSK5 &= ~(_BV(OCIE5C) | _BV(OCIE5B) | _BV(OCIE5A) | _BV(TOIE5 | _BV(ICIE5)));
#else
	TIMSK5 &= ~(_BV(OCIE5B) | _BV(OCIE5A) | _BV(TOIE5 | _BV(ICIE5)));
#endif
}

void kmTimer5RegisterCallbackCompA(void *userData, kmTimer5CallbackType *callback) {
	_timer5CallbackCompA = callback;
	_Timer5CallbackUserDataCompA = userData;
}

void kmTimer5RegisterCallbackCompB(void *userData, kmTimer5CallbackType *callback) {
	_timer5CallbackCompB = callback;
	_Timer5CallbackUserDataCompB = userData;
}

#ifdef OCR5C
void kmTimer5RegisterCallbackCompC(void *userData, kmTimer5CallbackType *callback) {
	_timer5CallbackCompC = callback;
	_Timer5CallbackUserDataCompC = userData;
}
#endif

void kmTimer5RegisterCallbackOVF(void *userData, kmTimer5CallbackType *callback) {
	_timer5CallbackOVF = callback;
	_timer5CallbackUserDataOVF = userData;
}

void kmTimer5RegisterCallbackInpCapture(void *userData, kmTimer5InpCaptureCallbackType *callback) {
	_timer5CallbackInpCapture = callback;
	_Timer5CallbackUserDataInpCapture = userData;
}

void kmTimer5UnregisterCallbackCompA(void) {
	_Timer5CallbackUserDataCompA = NULL;
}

void kmTimer5UnregisterCallbackCompB(void) {
	_Timer5CallbackUserDataCompB = NULL;
}

#ifdef OCR5C
void kmTimer5UnregisterCallbackCompC(void) {
	_Timer5CallbackUserDataCompC = NULL;
}
#endif

void kmTimer5UnregisterCallbackOVF(void) {
	_timer5CallbackUserDataOVF = NULL;
}

void kmTimer5UnregisterCallbackInpCapture(void) {
	_timer5CallbackInpCapture = NULL;
}

void kmTimer5SetCallbackUserDataOVF(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_timer5CallbackUserDataOVF = userData;
	}
}

void kmTimer5SetCallbackUserDataInpCapture(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer5CallbackUserDataInpCapture = userData;
	}
}

void kmTimer5SetCallbackUserDataCompA(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer5CallbackUserDataCompA = userData;
	}
}

void kmTimer5SetCallbackUserDataCompB(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer5CallbackUserDataCompB = userData;
	}
}

#ifdef OCR5C
void kmTimer5SetCallbackUserDataCompC(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_Timer5CallbackUserDataCompC = userData;
	}
}
#endif

bool kmTimer5InpCaputureGetState(void) {
	return (KM_TCC5_ICP5_PORT_INPUT & _BV(KM_TCC5_ICP5_PIN));
}

ISR(TIMER5_COMPA_vect, ISR_NOBLOCK) {
	if (NULL != _timer5CallbackCompA) {
		_timer5CallbackCompA(_Timer5CallbackUserDataCompA);
	}
}

ISR(TIMER5_COMPB_vect, ISR_NOBLOCK) {
	if (NULL != _timer5CallbackCompB) {
		_timer5CallbackCompB(_Timer5CallbackUserDataCompB);
	}
}

#ifdef OCR5C
ISR(TIMER5_COMPC_vect, ISR_NOBLOCK) {
	if (NULL != _timer5CallbackCompC) {
		_timer5CallbackCompC(_Timer5CallbackUserDataCompC);
	}
}
#endif

ISR(TIMER5_OVF_vect, ISR_NOBLOCK) {
	if (NULL != _timer5CallbackOVF) {
		_timer5CallbackOVF(_timer5CallbackUserDataOVF);
	}
}

ISR(TIMER5_CAPT_vect, ISR_NOBLOCK) {
	_inpCaptureCycles = ICR5;
	TCCR5B ^= _BV(ICES5);
	kmTimer5Restart();
	if (NULL != _timer5CallbackInpCapture) {
		_timer5CallbackInpCapture(_inpCaptureCycles, kmTimer5InpCaputureGetState(), _Timer5CallbackUserDataInpCapture);
	}
}

#endif /* TIMSK5 */
