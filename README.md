# ReadMe
# kmTimer5 library for AVR MCUs

The kmTimer5 library provides functions to initialize and control Timer 5 on AVR MCUs. It allows generating square waves with specified duty cycle and frequency, registering callbacks for overflow and comparator interrupts, enabling/disabling interrupts, configuring comparator outputs, controlling timer flow, and setting PWM duty cycles.

## Table of Contents
- [Version History](#version-history)
- [Overview](#overview)
- [Dependencies](#dependencies)
- [Usage](#usage)
- [Example Code](#example-code)
- [Author and License](#author-and-license)

## Version History
v1.0.0 Initial (2024-07-06)

## Overview
This library is designed for older models of AVR MCUs like ATmega8, ATmega32, etc. It initializes Timer 5 to generate square waves using Phase Accurate mode, allowing precise control over duty cycle and frequency. Callback functions can be registered for overflow and comparator interrupts, enabling users to execute custom routines at specific moments within Timer 5 periods.

## Dependencies
- [kmFrameworkAVR](https://gitlab.com/c-mos/kmAvrLibs)
  - [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)
  - [kmCpu](https://gitlab.com/c-mos/kmAvrLibs/kmCpu)
  - [kmTimersCommon](https://gitlab.com/c-mos/kmAvrLibs/kmTimersCommon)

## Usage
Getting this library and adding it to own project:
- To add this module to own project as submodule - enter the main directory of the source code and use git command

``` bash
git submodule add git@gitlab.com:c-mos/kmAvrLibs/kmTimer5.git kmTimer5
```
- After cloning own application from git repository use following additional git command to get correct revision of submodule:
``` bash
git submodule update --init
```

- Include the library in your AVR project.
- Initialize Timer 5 using `kmTimer5InitOnPrescalerBottomToTopPcPwmOCA`.
- Register callback functions for overflow and comparator interrupts using `kmTimer5RegisterCallbackOVF` and `kmTimer5RegisterCallbackCompA`, respectively.
- Enable interrupts using `kmTimer5EnableInterruptOVF` and `kmTimer5EnableInterruptCompA`.
- Configure comparator outputs using `kmTimer5ConfigureOCA`.
- Control timer flow using `kmTimer5Start`, `kmTimer5Stop`, and `kmTimer5Restart`.
- Set PWM duty cycles using `kmTimer5SetPwmDutyBottomToTop` and `kmTimer5SetPwmDutyAccurateTimeModes`.
- Include the library in your AVR project.
- Initialize Timer 5 using `kmTimer5InitOnPrescalerBottomToTopPcPwmOCA`.
- Register callback functions for overflow and comparator interrupts using `kmTimer5RegisterCallbackOVF` and `kmTimerrRegisterCallbackCompA`, respectively.
- Enable interrupts using `kmTimer5EnableInterruptOVF` and `kmTimer5EnableInterruptCompA`.
- Configure comparator outputs using `kmTimer5ConfigureOCA`.
- Control timer flow using `kmTimer5Start`, `kmTimer5Stop`, and `kmTimer5Restart`.
- Set PWM duty cycles using `kmTimer5SetPwmDutyBottomToTop` and `kmTimer5SetPwmDutyAccurateTimeModes`.


## Example Code
```c
#define KM_TIMER5_TEST_USER_DATA_A 1UL
#define KM_TIMER5_TEST_USER_DATA_B 65535UL
#define KM_TIMER5_TEST_USER_DATA_C 255UL
#define KM_TIMER5_TEST_DUTY_0_PERC KM_TIMER5_BOTTOM
#define KM_TIMER5_TEST_DUTY_25_PERC KM_TIMER5_MID - (KM_TIMER5_MID >> 1)
#define KM_TIMER5_TEST_DUTY_50_PERC KM_TIMER5_MID
#define KM_TIMER5_TEST_DUTY_75_PERC KM_TIMER5_MID + (KM_TIMER5_MID >> 1)
#define KM_TIMER5_TEST_DUTY_100_PERC KM_TIMER5_TOP

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"
#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer5/kmTimer5.h"

void callbackOVF(void *userData) {
    dbToggle(DB_PIN_0);
    dbOn(DB_PIN_1);
    dbOn(DB_PIN_2);
}

void callbackCompAOff(void *userData) {
    dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
    dbOff(DB_PIN_2);
}

int main(void) {
    appInitDebug();
    kmCpuInterruptsEnable();

    kmTimer5InitOnPrescalerBottomToTopOvfCompABInterruptCallback(KM_TCC0_PRSC_8);

    kmTimer5RegisterCallbackOVF(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_B), callbackOVF);
    kmTimer5EnableInterruptOVF();

    kmTimer5RegisterCallbackCompA(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_A), callbackCompAOff);
    kmTimer5SetValueCompA(KM_TIMER5_TEST_DUTY_25_PERC);
    kmTimer5EnableInterruptCompA();

    kmTimer5RegisterCallbackCompB(KM_TIMER5_USER_DATA(KM_TIMER5_TEST_USER_DATA_C), callbackCompBOff);
    kmTimer5SetValueCompB(KM_TIMER5_TEST_DUTY_75_PERC);
    kmTimer5EnableInterruptCompB();

    kmTimer5Start();

    while(true) {
    }
}
```

See Also:
[kmTimer5 Example Test Application](https://gitlab.com/c-mos/kmAvrTests/kmTimer5Test)

## Author and License
**Author**: Krzysztof Moskwa

**e-mail**: chris[dot]moskva[at]gmail[dot]com

**License**: GPL-3.0-or-later

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt)

 ![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)
